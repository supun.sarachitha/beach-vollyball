﻿using BeachVollyBall.Model;
using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeachVollyBall.Data
{
    public class MainDataBase
    {
        readonly SQLiteAsyncConnection _database;

        public MainDataBase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Team>().Wait();
            _database.CreateTableAsync<GameModel>().Wait();
            _database.CreateTableAsync<SetsModel>().Wait();
            _database.CreateTableAsync<StatsModel>().Wait();
        }


        //Team
        public Task<int> SaveTeamAsync(Team team)
        {
            if (team.TeamID != 0)
            {
                return _database.UpdateAsync(team);
            }
            else
            {
                return _database.InsertAsync(team);
            }
        }

        internal Task<List<Team>> GetTeamsAsync()
        {
            return _database.Table<Team>().ToListAsync();
        }


        public Task<int> DeleteTeamAsync(Team team)
        {
            return _database.DeleteAsync(team);
        }


        //Game
        public Task<int> SaveGameAsync(GameModel game)
        {
            
                return _database.InsertAsync(game);
        }

        internal Task<List<GameModel>> GetGamesAsync()
        {
            return _database.Table<GameModel>().ToListAsync();
        }


        public Task<int> DeleteGameAsync(GameModel game)
        {
            return _database.DeleteAsync(game);
        }



        //Sets
        public Task<int> SaveSetsAsync(SetsModel set)
        {
            return _database.InsertAsync(set);
        }

        internal Task<List<SetsModel>> GetSetsAsync()
        {
            return _database.Table<SetsModel>().ToListAsync();
        }


        public Task<bool>  DeleteSetsAsync(int ID)
        {
            _database.QueryAsync<SetsModel>(
                    @"DELETE from SetsModel where GameID=", ID);
           
            return Task.FromResult(true);
           
        }


        //Stats
        public Task<int> SaveStatsAsync(StatsModel set)
        {
            return _database.InsertAsync(set);
        }

        internal Task<List<StatsModel>> GetStatsAsync()
        {
            return _database.Table<StatsModel>().ToListAsync();
        }


        public Task<bool> DeleteStatsAsync(int Id)
        {
            _database.QueryAsync<StatsModel>(
                     @"DELETE from StatsModel where GameID=", Id);

            return Task.FromResult(true);
        }


        //history
        internal Task<List<HistoryModel>> GetHistoryAsync()
        {
            var q = _database.QueryAsync<HistoryModel>(
                    @"SELECT G.GameID, G.TeamID_1,G.TeamID_2,G.PlayDate,S.Team_1_points, S.Team_2_points FROM GameModel G LEFT JOIN SetsModel S ON G.GameID =S.GameID");

            return q;
        }

    }
}
