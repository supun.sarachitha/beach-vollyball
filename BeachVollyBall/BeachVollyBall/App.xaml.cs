﻿using BeachVollyBall.Data;
using BeachVollyBall.View;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


[assembly: ExportFont("fa-brands-400.ttf", Alias = "fa-brands")]
[assembly: ExportFont("fa-regular-400.ttf", Alias = "fa-regular")]
[assembly: ExportFont("fa-solid-900.ttf", Alias = "fa-solid")]
[assembly: ExportFont("RussoOne-Regular.ttf", Alias = "MainFont")]
namespace BeachVollyBall
{
    public partial class App : Application
    {

        static MainDataBase database;

        public static MainDataBase Database
        {
            get
            {
                if (database == null)
                {
                    database = new MainDataBase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Notes.db3"));
                }
                return database;
            }
        }

        public App()
        {
            InitializeComponent();

            MainPage = new HomePage();

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
