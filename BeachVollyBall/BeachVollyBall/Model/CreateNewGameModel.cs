﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class CreateNewGameModel
    {

        public string Description { get; set; }
        public int Team1 {get;set;}
        public int Team2 { get; set; }

        public string Team1Name { get; set; }
        public string Team2Name { get; set; }
        public bool IsCustom { get; set; }

    }
}
