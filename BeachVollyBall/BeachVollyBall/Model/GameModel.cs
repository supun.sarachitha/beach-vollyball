﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class GameModel
    {
        [PrimaryKey, AutoIncrement]
        public int GameID { get; set; }
        public string Description { get; set; }
        public int GameWinner { get; set; }
        public int TeamID_1 { get; set; }
        public int TeamID_2 { get; set; }
        public string PlayTime { get; set; }
        public DateTime PlayDate { get; set; }
        public bool isCustom { get; set; }
        public bool isFF { get; set; }

        //these below param uses for histroy page
        public string Team1Name { get; set; }

        public string Team2Name { get; set; }

        public string setScore { get; set; }

    }
}
