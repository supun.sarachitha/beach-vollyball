﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class Team
    {
        [PrimaryKey, AutoIncrement]
        public int TeamID { get; set; }
        public string Name { get; set; }
        public string PlayerName1 { get; set; }
        public string PlayerName2 { get; set; }

    }
}
