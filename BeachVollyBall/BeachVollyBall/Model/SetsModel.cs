﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class SetsModel
    {
        public int SetsID { get; set; }
        public int GameID { get; set; }
        public int SetWinner { get; set; }

        public int Team_1_points { get; set; }
        public int Team_2_points { get; set; }
        public string PlayTime { get; set; }
        public int Replay { get; set; }

    }
}
