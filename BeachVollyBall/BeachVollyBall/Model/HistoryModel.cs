﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class HistoryModel
    {
        public int GameID { get; set; }
        public string Description { get; set; }
        public int GameWinner { get; set; }
        public int TeamID_1 { get; set; }
        public int TeamID_2 { get; set; }
        public string Team1Name { get; set; }
        public string Team2Name { get; set; }
        public string PlayTime { get; set; }
        public DateTime PlayDate { get; set; }
        public bool isCustom { get; set; }
        public bool isFF { get; set; }

    }
}
