﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeachVollyBall.Model
{
    public class StatsModel
    {
        [PrimaryKey, AutoIncrement]
        public int StatID { get; set; }
        public int GameID { get; set; }
        public int SetID { get; set; }
        public int TeamID { get; set; }
        public int Points { get; set; }
        public int Serve { get; set; }
        public int Serve_Pt { get; set; }
        public int Serve_Ace { get; set; }
        public int Serve_Err { get; set; }
        public int Attack { get; set; }
        public int Attack_Err { get; set; }
        public int Attack_Pt { get; set; }
        public int Attack_Kill { get; set; }
        public int Receive { get; set; }
        public int Receive_Err { get; set; }
        public int Block { get; set; }
        public int Block_Err { get; set; }
        public int Block_Pt { get; set; }
        public int Fault { get; set; }
        public int Error_InPlay { get; set; }
    }
}
