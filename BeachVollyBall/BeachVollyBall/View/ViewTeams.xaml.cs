﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewTeams : ContentPage
    {
        public ViewTeams()
        {
            InitializeComponent();

        }

        private async void BindData()
        {
            try
            {
                listview.ItemsSource = await App.Database.GetTeamsAsync();
            }
            catch (Exception)
            {

                return;
            }

        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }



        private void btnAddNew_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AddNewTemaPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindData();
        }


        Team selectedObject = null;
        private void teamTapped_Tapped(object sender, EventArgs e)
        {
            Grid button = (Grid)sender;
            selectedObject = (Team)button.BindingContext;
            btndelete.IsEnabled = true;

        }

        private async void btndelete_Clicked(object sender, EventArgs e)
        {
            if (selectedObject != null)
            {
                bool res = await DisplayAlert("", "Delete team : " + selectedObject.Name + "?", "Yes", "No");
                if (res)
                {
                    await App.Database.DeleteTeamAsync(selectedObject);
                    btndelete.IsEnabled = false;
                    BindData();
                }
                    
            }

        }
    
    }
}