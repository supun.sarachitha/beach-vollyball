﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        //initializing game point variables 
        int ServeAPosints = 0;
        int ServeBPosints = 0;
        int AttackAPoints = 0;
        int AttackBPoints = 0;
        int ReceiveAPoints = 0;
        int ReceiveBPoints = 0;
        int BlockAPoints = 0;
        int BlockBPoints = 0;
        int FaultAPoints = 0;
        int FaultBPoints = 0;

        int ServeA_pt = 0;
        int ServeA_ace = 0;
        int ServeA_err = 0;
        int AttackA_err = 0;
        int AttackA_pt = 0;
        int AttackA_kill = 0;
        int ReceiveA_err = 0;
        int BlockA_err = 0;
        int BlockA_pt = 0;
        int ErrorA_inPlay = 0;

        int ServeB_pt = 0;
        int ServeB_ace = 0;
        int ServeB_err = 0;
        int AttackB_err = 0;
        int AttackB_pt = 0;
        int AttackB_kill = 0;
        int ReceiveB_err = 0;
        int BlockB_err = 0;
        int BlockB_pt = 0;
        int ErrorB_inPlay = 0;

        int currentGameId = 0;

        bool isfreezed = false; //variable to checkk whether game is freezed or not


        //double/single tap lock variables
        bool SingleTapServeA = false;
        bool DoubleTapServeA = false;
        bool SingleTapServeB = false;
        bool DoubleTapServeB = false;

        bool SingleTapAttackA = false;
        bool DoubleTapAttackA = false;
        bool SingleTapAttackB = false;
        bool DoubleTapAttackB = false;

        bool SingleTapReciveA = false;
        bool DoubleTapReciveA = false;
        bool SingleTapReciveB = false;
        bool DoubleTapReciveB = false;

        bool SingleTapBlockA = false;
        bool DoubleTapBlockA = false;
        bool SingleTapBlockB = false;
        bool DoubleTapBlockB = false;

        bool SingleTapFaultA = false;
        bool SingleTapFaultB = false;

        bool GameFinished = false; //check game status when click back button

        public GamePage()
        {
            InitializeComponent();

            Set1startTime = DateTime.Now; //save set 1 start time to a varialble
            StartTimer(); //start set timer
            Currentset = 1; // assign current set value to variable

            //assign team names to score board
            txtTeamAName.Text = Common.newgame.Team1Name;
            txtTeamBName.Text = Common.newgame.Team2Name;


            //getGameId
            GetGameID();

            // ======single and double tap events in Game Page===//

            //=============serve=============//
            //team A serve single tap event
            var OneTapServeA = new TapGestureRecognizer();
            OneTapServeA.NumberOfTapsRequired = 1;
            OneTapServeA.Tapped += async (s, e) =>
            {

                if (!SingleTapServeA) //checking serve button is enable or not
                {
                    ServeAPosints = ServeAPosints + 1;
                    txtServeAPosints.Text = (ServeAPosints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapServeA = true; //lock current button
                    txtLastAction.Text = "Team A Serve";
                }
            };
            btnServeA.GestureRecognizers.Add(OneTapServeA);

            //team A serve Double tap event
            var TwoTapServeA = new TapGestureRecognizer();
            TwoTapServeA.NumberOfTapsRequired = 2;
            TwoTapServeA.Tapped += async (s, e) =>
            {

                if (!DoubleTapServeA) //checking serve button is enable or not
                {
                    string action = await DisplayActionSheet("Please select type of Serve", "Cancel", null, "Serve error", "Serve ace", "Serve point");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapServeA = true; //lock current button

                    txtLastAction.Text = "Team A " + action;

                    //Serve_Ace smallint, // earns +1 point to respective team, also earns +1 serve_pt
                    if (action == "Serve ace")
                    {
                        ServeA_ace = ServeA_ace + 1;
                        addTeamAPoint();
                    }

                    // earns +1point to opposing team
                    if (action == "Serve error")
                    {
                        ServeA_err = ServeA_err + 1;
                        addTeamBPoint();
                    }

                    // Serve pt is also when either respective team gets serve ace OR opposing team fails to receive
                    if (action == "Serve point")
                    {
                        ServeA_pt = ServeA_pt + 1;
                        addTeamAPoint();
                    }
                }

            };
            btnServeA.GestureRecognizers.Add(TwoTapServeA);

            //team B serve single tap event
            var OneTapServeB = new TapGestureRecognizer();
            OneTapServeB.NumberOfTapsRequired = 1;
            OneTapServeB.Tapped += async (s, e) =>
            {
                if (!DoubleTapServeB) //checking serve button is enable or not
                {
                    ServeBPosints = ServeBPosints + 1;
                    txtServeBPosints.Text = (ServeBPosints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapServeB = true; //lock current button
                    txtLastAction.Text = "Team B Serve";
                }

            };
            btnServeB.GestureRecognizers.Add(OneTapServeB);

            //team B serve Double tap event
            var TwoTapServeB = new TapGestureRecognizer();
            TwoTapServeB.NumberOfTapsRequired = 2;
            TwoTapServeB.Tapped += async (s, e) =>
            {
                if (!SingleTapServeB) //checking serve button is enable or not
                {
                    string action = await DisplayActionSheet("Please select type of Serve", "Cancel", null, "Serve error", "Serve ace", "Serve point");

                    releaseDoubleTap(); //release all button locking
                    DoubleTapServeB = true; //lock current button
                    txtLastAction.Text = "Team B " + action;

                    //Serve_Ace smallint, // earns +1 point to respective team, also earns +1 serve_pt
                    if (action == "Serve ace")
                    {
                        ServeB_ace = ServeB_ace + 1;
                        addTeamBPoint();
                    }

                    // earns +1point to opposing team
                    if (action == "Serve error")
                    {
                        ServeB_err = ServeB_err + 1;
                        addTeamAPoint();
                    }

                    // Serve pt is also when either respective team gets serve ace OR opposing team fails to receive
                    if (action == "Serve point")
                    {
                        ServeB_pt = ServeB_pt + 1;
                        addTeamBPoint();
                    }
                }

            };
            btnServeB.GestureRecognizers.Add(TwoTapServeB);




            //===============attack======================//
            //team A attack single tap event
            var OneTapAttackA = new TapGestureRecognizer();
            OneTapAttackA.NumberOfTapsRequired = 1;
            OneTapAttackA.Tapped += async (s, e) =>
            {
                if (!SingleTapAttackA)
                {
                    AttackAPoints = AttackAPoints + 1;
                    txtAttackAPosints.Text = (AttackAPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapAttackA = true; //lock current button
                    txtLastAction.Text = "Team A Attack";
                }

            };
            btnAttackA.GestureRecognizers.Add(OneTapAttackA);

            //team A attack Double tap event
            var TwoTapAttackA = new TapGestureRecognizer();
            TwoTapAttackA.NumberOfTapsRequired = 2;
            TwoTapAttackA.Tapped += async (s, e) =>
            {
                if (!DoubleTapAttackA)
                {
                    string action = await DisplayActionSheet("Please select type of Attack", "Cancel", null, "Attack error", "Attack_pt", "Attack_kill");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapAttackA = true; //lock current button
                    txtLastAction.Text = "Team A " + action;

                    //Attack_Err smallint, // earns +1point to opposing team
                    if (action == "Attack error")
                    {
                        AttackA_err = AttackA_err + 1;
                        addTeamBPoint();
                    }

                    //Attack_Kill smallint, // earns +1 point to respective team, also earns +1 attack_pt
                    if (action == "Attack_kill")
                    {
                        AttackA_kill = AttackA_kill + 1;
                        addTeamAPoint();
                    }

                    if (action == "Attack_pt")
                    {
                        AttackA_pt = AttackA_pt + 1;

                    }




                }
            };
            btnAttackA.GestureRecognizers.Add(TwoTapAttackA);

            //team B attack single tap event
            var OneTapAttackB = new TapGestureRecognizer();
            OneTapAttackB.NumberOfTapsRequired = 1;
            OneTapAttackB.Tapped += async (s, e) =>
            {
                if (!SingleTapAttackB)
                {
                    AttackBPoints = AttackBPoints + 1;
                    txtAttackBPosints.Text = (AttackBPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapAttackB = true; //lock current button
                    txtLastAction.Text = "Team B Attack";
                }

            };
            btnAttackB.GestureRecognizers.Add(OneTapAttackB);

            //team B attack Double tap event
            var TwoTapAttackB = new TapGestureRecognizer();
            TwoTapAttackB.NumberOfTapsRequired = 2;
            TwoTapAttackB.Tapped += async (s, e) =>
            {
                if (!DoubleTapAttackB)
                {
                    string action = await DisplayActionSheet("Please select type of Attack", "Cancel", null, "Attack error", "Attack_pt", "Attack_kill");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapAttackB = true; //lock current button
                    txtLastAction.Text = "Team B " + action;

                    //Attack_Err smallint, // earns +1point to opposing team
                    if (action == "Attack error")
                    {
                        AttackB_err = AttackB_err + 1;
                        addTeamAPoint();
                    }

                    //Attack_Kill smallint, // earns +1 point to respective team, also earns +1 attack_pt
                    if (action == "Attack_kill")
                    {
                        AttackB_kill = AttackB_kill + 1;
                        addTeamBPoint();
                    }

                    if (action == "Attack_pt")
                    {
                        AttackA_pt = AttackA_pt + 1;

                    }
                }
            };
            btnAttackB.GestureRecognizers.Add(TwoTapAttackB);




            //============Receive============//
            //team A Receive single tap event
            var OneTapReceiveA = new TapGestureRecognizer();
            OneTapReceiveA.NumberOfTapsRequired = 1;
            OneTapReceiveA.Tapped += async (s, e) =>
            {
                if (!SingleTapReciveA)
                {
                    ReceiveAPoints = ReceiveAPoints + 1;
                    txtReceiveAPosints.Text = (ReceiveAPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapReciveA = true; //lock current button
                    txtLastAction.Text = "Team A Recieve";
                }

            };
            btnReceiveA.GestureRecognizers.Add(OneTapReceiveA);

            //team A Receive Double tap event
            var TwoTapReceiveA = new TapGestureRecognizer();
            TwoTapReceiveA.NumberOfTapsRequired = 2;
            TwoTapReceiveA.Tapped += async (s, e) =>
            {
                if (!DoubleTapReciveA)
                {
                    string action = await DisplayActionSheet("Please select type of Receive", "Cancel", null, "Receive error");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapReciveA = true; //lock current button


                    // Receive_Err smallint, // earns +1point to opposing team, also earns +1 attack
                    if (action == "Receive error")
                    {
                        ReceiveA_err = ReceiveA_err + 1;
                        addTeamBPoint();
                    }

                    //pt / serve pt to opposing team(if attack / serve was previous action before this error(even when user has pressed receive button))
                    if (txtLastAction.Text == "Team B Attack" || txtLastAction.Text == "Team B Serve")
                    {
                        addTeamBPoint();
                    }

                    txtLastAction.Text = "Team A " + action;
                }
            };
            btnReceiveA.GestureRecognizers.Add(TwoTapReceiveA);

            //team B Receive single tap event
            var OneTapReceiveB = new TapGestureRecognizer();
            OneTapReceiveB.NumberOfTapsRequired = 1;
            OneTapReceiveB.Tapped += async (s, e) =>
            {
                if (!SingleTapReciveB)
                {
                    ReceiveBPoints = ReceiveBPoints + 1;
                    txtReceiveBPosints.Text = (ReceiveBPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapReciveB = true; //lock current button
                    txtLastAction.Text = "Team A Recieve";
                }

            };
            btnReceiveB.GestureRecognizers.Add(OneTapReceiveB);

            //team B Receive Double tap event
            var TwoTapReceiveB = new TapGestureRecognizer();
            TwoTapReceiveB.NumberOfTapsRequired = 2;
            TwoTapReceiveB.Tapped += async (s, e) =>
            {
                if (!DoubleTapReciveB)
                {
                    string action = await DisplayActionSheet("Please select type of Receive", "Cancel", null, "Receive error");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapReciveB = true; //lock current button


                    // Receive_Err smallint, // earns +1point to opposing team, also earns +1 attack
                    if (action == "Receive error")
                    {
                        ReceiveB_err = ReceiveB_err + 1;
                        addTeamAPoint();
                    }

                    //pt / serve pt to opposing team(if attack / serve was previous action before this error(even when user has pressed receive button))
                    if (txtLastAction.Text == "Team A Attack" || txtLastAction.Text == "Team A Serve")
                    {
                        addTeamAPoint();
                    }

                    txtLastAction.Text = "Team B " + action;
                }

            };
            btnReceiveB.GestureRecognizers.Add(TwoTapReceiveB);




            //============Block============//
            //team A Block single tap event
            var OneTapBlockA = new TapGestureRecognizer();
            OneTapBlockA.NumberOfTapsRequired = 1;
            OneTapBlockA.Tapped += async (s, e) =>
            {
                if (!SingleTapBlockA)
                {
                    BlockAPoints = BlockAPoints + 1;
                    txtBlockAPosints.Text = (BlockAPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapBlockA = true; //lock current button
                    txtLastAction.Text = "Team A Block";
                }

            };
            btnBlockA.GestureRecognizers.Add(OneTapBlockA);

            //team A Block Double tap event
            var TwoTapBlockA = new TapGestureRecognizer();
            TwoTapBlockA.NumberOfTapsRequired = 2;
            TwoTapBlockA.Tapped += async (s, e) =>
            {
                if (!DoubleTapBlockA)
                {
                    string action = await DisplayActionSheet("Please select type of Block", "Cancel", null, " Block error", "Block_pt");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapBlockA = true; //lock current button
                    txtLastAction.Text = "Team A " + action;


                    //Block_Err smallint, // earns +1point to opposing team, also earns +1 attack pt
                    if (action == "Block error")
                    {
                        BlockA_err = BlockA_err + 1;
                        addTeamBPoint();
                    }

                    //Block_Pt smallint, // earns +1 point to respective team
                    if (action == "Block_pt")
                    {
                        BlockA_pt = BlockA_pt + 1;
                        addTeamBPoint();
                    }
                }



            };
            btnBlockA.GestureRecognizers.Add(TwoTapBlockA);

            //team B Block single tap event
            var OneTapBlockB = new TapGestureRecognizer();
            OneTapBlockB.NumberOfTapsRequired = 1;
            OneTapBlockB.Tapped += async (s, e) =>
            {
                if (!SingleTapBlockB)
                {
                    BlockBPoints = BlockBPoints + 1;
                    txtBlockBPosints.Text = (BlockBPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapBlockB = true; //lock current button
                    txtLastAction.Text = "Team B Block";
                }

            };
            btnBlockB.GestureRecognizers.Add(OneTapBlockB);

            //team B Block Double tap event
            var TwoTapBlockB = new TapGestureRecognizer();
            TwoTapBlockB.NumberOfTapsRequired = 2;
            TwoTapBlockB.Tapped += async (s, e) =>
            {
                if (!DoubleTapBlockB)
                {
                    string action = await DisplayActionSheet("Please select type of Block", "Cancel", null, " Block error", "Block_pt");
                    releaseDoubleTap(); //release all button locking
                    DoubleTapBlockB = false; //lock current button
                    txtLastAction.Text = "Team B " + action;

                    //Block_Err smallint, // earns +1point to opposing team, also earns +1 attack pt
                    if (action == "Block error")
                    {
                        BlockB_err = BlockB_err + 1;
                        addTeamAPoint();
                    }

                    //Block_Pt smallint, // earns +1 point to respective team
                    if (action == "Block_pt")
                    {
                        BlockB_pt = BlockB_pt + 1;
                        addTeamBPoint();
                    }
                }
            };
            btnBlockB.GestureRecognizers.Add(TwoTapBlockB);




            //============Fault============//
            //team A Fault single tap event
            var OneTapFaultA = new TapGestureRecognizer();
            OneTapFaultA.NumberOfTapsRequired = 1;
            OneTapFaultA.Tapped += async (s, e) =>
            {
                if (!SingleTapFaultA)
                {
                    FaultAPoints = FaultAPoints + 1;
                    txtFaultAPosints.Text = (FaultAPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapFaultA = true; //lock current button
                    txtLastAction.Text = "Team A Fault";

                    //Fault smallint, // earns +1point to opposing team
                    addTeamBPoint();
                }

            };
            btnFaultA.GestureRecognizers.Add(OneTapFaultA);



            //team B Fault single tap event
            var OneTapFaultB = new TapGestureRecognizer();
            OneTapFaultB.NumberOfTapsRequired = 1;
            OneTapFaultB.Tapped += async (s, e) =>
            {
                if (!SingleTapFaultB)
                {
                    FaultBPoints = FaultBPoints + 1;
                    txtFaultBPosints.Text = (FaultBPoints).ToString();
                    releaseSingleTap(); //release all button locking
                    SingleTapFaultB = true; //lock current button
                    txtLastAction.Text = "Team B Fault";

                    //Fault smallint, // earns +1point to opposing team
                    addTeamAPoint();
                }

            };
            btnFaultB.GestureRecognizers.Add(OneTapFaultB);



        }

        private void releaseSingleTap()
        {
            SingleTapServeA = false;
            SingleTapServeB = false;
            SingleTapAttackA = false;
            SingleTapAttackB = false;
            SingleTapBlockA = false;
            SingleTapBlockB = false;
            SingleTapFaultA = false;
            SingleTapFaultB = false;
            SingleTapReciveA = false;
            SingleTapReciveB = false;

        }

        private void releaseDoubleTap()
        {
            DoubleTapServeA = false;
            DoubleTapServeB = false;
            DoubleTapAttackA = false;
            DoubleTapAttackB = false;
            DoubleTapBlockA = false;
            DoubleTapBlockB = false;
            DoubleTapReciveA = false;
            DoubleTapReciveB = false;

        }


        //get final Game ID
        private async void GetGameID()
        {
            var gameList = await App.Database.GetGamesAsync();
            if (gameList.Count > 0)
            {
                currentGameId = gameList.Max(x => x.GameID) + 1;
            }
            else
            {
                currentGameId = 1;
            }


        }

        DateTime Set1startTime;
        DateTime Set2startTime;
        DateTime Set3startTime;
        private void StartTimer()
        {
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Device.BeginInvokeOnMainThread(() =>

                    {
                        if (Currentset == 0)
                        {

                        }
                        else if (Currentset == 1)
                        {
                            txtSet1Timer.Text = (DateTime.Now - Set1startTime).Minutes.ToString() + ":" + (DateTime.Now - Set1startTime).Seconds.ToString();
                        }
                        else if (Currentset == 2)
                        {
                            txtSet2Timer.Text = (DateTime.Now - Set2startTime).Minutes.ToString() + ":" + (DateTime.Now - Set2startTime).Seconds.ToString();
                        }
                        else if (Currentset == 3)
                        {
                            txtSet3Timer.Text = (DateTime.Now - Set3startTime).Minutes.ToString() + ":" + (DateTime.Now - Set3startTime).Seconds.ToString();
                        }

                    }
                );

                return true; // True = Repeat again, False = Stop the timer
            });
        }

        private void releaseButtons()
        {
            btnAttackA.IsEnabled = true;
            btnAttackB.IsEnabled = true;
            btnBlockA.IsEnabled = true;
            btnBlockB.IsEnabled = true;
            btnFaultA.IsEnabled = true;
            btnFaultB.IsEnabled = true;
            btnReceiveA.IsEnabled = true;
            btnReceiveB.IsEnabled = true;
            btnServeA.IsEnabled = true;
            btnServeB.IsEnabled = true;

            DoubleTapServeA = false;
            SingleTapServeA = false;

        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {

            if (!GameFinished)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var res = await DisplayAlert("", "Do you want to abandon the game?", "Yes", "No");
                    if (res)
                    {
                        await Navigation.PopModalAsync();
                    }
                });

            }
            else
            {
                await Navigation.PopModalAsync();
            }



        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }

        private void TapGestureRecognizer1_Tapped(object sender, EventArgs e)
        {

        }

        private void TapGestureRecognizer2_Tapped(object sender, EventArgs e)
        {

        }


        int TeamASet1 = 0;
        int TeamBSet1 = 0;
        int TeamASet2 = 0;
        int TeamBSet2 = 0;
        int TeamASet3 = 0;
        int TeamBSet3 = 0;
        public void addTeamAPoint()
        {
            if (Currentset == 1)
            {
                TeamASet1 = TeamASet1 + 1;
                txtTeamASet1.Text = (TeamASet1).ToString();
                checkFinalResult();
            }
            else if (Currentset == 2)
            {
                TeamASet2 = TeamASet2 + 1;
                txtTeamASet2.Text = (TeamASet2).ToString();
                checkFinalResult();
            }
            else if (Currentset == 3)
            {
                TeamASet3 = TeamASet3 + 1;
                txtTeamASet3.Text = (TeamASet3).ToString();
                checkFinalResult();
            }

        }



        public void addTeamBPoint()
        {

            if (Currentset == 1)
            {
                TeamBSet1 = TeamBSet1 + 1;
                txtTeamBSet1.Text = (TeamBSet1).ToString();
                checkFinalResult();
            }
            else if (Currentset == 2)
            {
                TeamBSet2 = TeamBSet2 + 1;
                txtTeamBSet2.Text = (TeamBSet2).ToString();
                checkFinalResult();
            }
            else if (Currentset == 3)
            {
                TeamBSet3 = TeamBSet3 + 1;
                txtTeamBSet3.Text = (TeamBSet3).ToString();
                checkFinalResult();
            }
        }


        //Final decision logic implemented in this function 

        int teamAWinCount = 0;
        int teamBWinCount = 0;
        int Currentset = 0;
        private async void checkFinalResult()
        {
            //Classic means 3 sets are to be played - 1st set, 21 points; 2nd set, 21 points, 3rd set 15 points.
          
                //if current set 1
                if (Currentset == 1)
                {

                    //check one of team reached to point 21
                    if (Convert.ToInt32(txtTeamASet1.Text) > 20 || Convert.ToInt32(txtTeamBSet1.Text) > 20)
                    {
                        //check team A has 2 point advantage
                        if ((Convert.ToInt32(txtTeamASet1.Text) - (Convert.ToInt32(txtTeamBSet1.Text))) >1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + " won set 1", "Ok");
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                        else if ((Convert.ToInt32(txtTeamBSet1.Text) - (Convert.ToInt32(txtTeamASet1.Text))) > 1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team2Name + " won set 1", "Ok");
                            teamBWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                    }


                }

                //if current  set  2
                if (Currentset == 2)
                {
                    //check one of team reached to point 21
                    if (Convert.ToInt32(txtTeamASet2.Text) > 20 || Convert.ToInt32(txtTeamBSet2.Text) > 20)
                    {
                        //check team A has 2 point advantage
                        if ((Convert.ToInt32(txtTeamASet2.Text) - (Convert.ToInt32(txtTeamBSet2.Text))) > 1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + " won set 2", "Ok");
                            teamAWinCount = teamAWinCount + 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 1, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }
                        else if ((Convert.ToInt32(txtTeamBSet2.Text) - (Convert.ToInt32(txtTeamASet2.Text))) >1 )
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team2Name + " won set 2", "Ok");
                            teamBWinCount = teamBWinCount + 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }

                        //check either of team has +2 point differential
                        if (teamAWinCount - teamBWinCount == 2)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
                            disabletaps();
                            SaveSetsToDatabase(2, 1, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                            //save game
                            saveToDataBase(1);
                        }
                        else if (teamBWinCount - teamAWinCount == 2)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                            //save game
                            saveToDataBase(2);
                        }

                    }
                }

                // if current set 3
                if (Currentset == 3)
                {

                if (!Common.newgame.IsCustom)
                {
                    //check one of team reached to point 15
                    if (Convert.ToInt32(txtTeamASet3.Text) > 14 || Convert.ToInt32(txtTeamBSet3.Text) > 14)
                    {
                        //check team A has 2 point advantage
                        if ((Convert.ToInt32(txtTeamASet3.Text) - (Convert.ToInt32(txtTeamBSet3.Text))) >1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + "won set 2", "Ok");
                            Currentset = 0;
                            teamAWinCount = teamAWinCount + 1;

                            //decides who won the game
                            if (teamAWinCount > teamBWinCount)
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
                                disabletaps();

                                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(1);

                            }
                            else
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(2);

                            }
                        }

                        //check team B has 2 point advantage
                        if ((Convert.ToInt32(txtTeamBSet3.Text) - (Convert.ToInt32(txtTeamASet3.Text))) >1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + " won set 2", "Ok");
                            Currentset = 0;
                            teamBWinCount = teamBWinCount + 1;

                            //decides who won the game
                            if (teamAWinCount > teamBWinCount)
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(1);
                            }
                            else
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(2);
                            }
                        }
                    }


                }

                else if (Common.newgame.IsCustom) // if cutom game
                {
                    //check one of team reached to point 15
                    if (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20)
                    {
                        //check team A has 2 point advantage
                        if ((Convert.ToInt32(txtTeamASet3.Text) - (Convert.ToInt32(txtTeamBSet3.Text))) > 1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + "won set 2", "Ok");
                            Currentset = 0;
                            teamAWinCount = teamAWinCount + 1;

                            //decides who won the game
                            if (teamAWinCount > teamBWinCount)
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
                                disabletaps();

                                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(1);

                            }
                            else
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(2);

                            }
                        }

                        //check team B has 2 point advantage
                        if ((Convert.ToInt32(txtTeamBSet3.Text) - (Convert.ToInt32(txtTeamASet3.Text))) >1)
                        {
                            await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + " won set 2", "Ok");
                            Currentset = 0;
                            teamBWinCount = teamBWinCount + 1;

                            //decides who won the game
                            if (teamAWinCount > teamBWinCount)
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(1);
                            }
                            else
                            {
                                await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                                disabletaps();
                                SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                                //save game
                                saveToDataBase(2);
                            }
                        }
                    }


                }



            }


           


            //old code for reference. remove before final release
            #region
            //if custom game? all sets are being played till 21 points/+2 point differential
            //else
            //{


            //    //check either of team has +2 point differential
            //    if (Currentset == 1)
            //    {
            //        if (teamAWinCount - teamBWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
            //            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

            //            var res = await DisplayAlert("", "Do you want to continue the game?", "Cancel", "Ok");
            //            if (res)
            //            {
            //                disabletaps();
            //                //save game
            //                saveToDataBase(1);
            //            }

            //        }
            //        else if (teamBWinCount - teamAWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team  " + Common.newgame.Team2Name + "  has won the Game", "Ok");
            //            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

            //            var res = await DisplayAlert("", "Do you want to continue the game?", "Cancel", "Ok");
            //            if (res)
            //            {
            //                disabletaps();
            //                //save game
            //                saveToDataBase(2);
            //            }
            //        }




            //        //check one of team reached to point 21
            //        if (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20)
            //        {
            //            //check team A has 2 point advantage
            //            if ((Convert.ToInt32(txtTeamASet3.Text) - (Convert.ToInt32(txtTeamBSet3.Text))) == 2)
            //            {
            //                await DisplayAlert("Set Complete", "Team " + Common.newgame.Team1Name + " won set 1", "Ok");
            //                teamAWinCount = 1;
            //                ResetGameBoard();
            //                SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
            //            }
            //            else if ((Convert.ToInt32(txtTeamBSet3.Text) - (Convert.ToInt32(txtTeamASet3.Text))) == 2)
            //            {
            //                await DisplayAlert("Set Complete", "Team " + Common.newgame.Team2Name + " won set 1", "Ok");
            //                teamBWinCount = 1;
            //                ResetGameBoard();
            //                SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
            //            }
            //        }






            //    }
            //    else if (Currentset == 2)
            //    {
            //        if (teamAWinCount - teamBWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
            //            SaveSetsToDatabase(2, 1, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

            //            var res = await DisplayAlert("", "Do you want to continue the game?", "Cancel", "Ok");
            //            if (res)
            //            {
            //                disabletaps();
            //                //save game
            //                saveToDataBase(1);
            //            }

            //        }
            //        else if (teamBWinCount - teamAWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team  " + Common.newgame.Team2Name + "  has won the Game", "Ok");
            //            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

            //            var res = await DisplayAlert("", "Continue the game?", "Cancel", "Ok");
            //            if (res)
            //            {
            //                disabletaps();
            //                //save game
            //                saveToDataBase(2);
            //            }
            //        }
            //    }
            //    else if (Currentset == 3)
            //    {
            //        if (teamAWinCount - teamBWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team " + Common.newgame.Team1Name + " has won the Game", "Ok");
            //            SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));


            //            disabletaps();
            //            //save game
            //            saveToDataBase(1);

            //        }
            //        else if (teamBWinCount - teamAWinCount == 2)
            //        {
            //            await DisplayAlert("", "Team  " + Common.newgame.Team2Name + "  has won the Game", "Ok");
            //            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

            //            disabletaps();
            //            //save game
            //            saveToDataBase(2);
            //        }
            //        else if ((Convert.ToInt32(txtTeamASet3.Text) > 21 || (Convert.ToInt32(txtTeamBSet3.Text)) > 21))
            //        {
            //            if ((Convert.ToInt32(txtTeamASet3.Text) - (Convert.ToInt32(txtTeamBSet3.Text))) == 2)
            //            {
            //                await DisplayAlert("", "Team   " + Common.newgame.Team2Name + "   has won the Game", "Ok");
            //                disabletaps();
            //                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
            //                //save game
            //                saveToDataBase(1);
            //            }
            //            else if ((Convert.ToInt32(txtTeamBSet3.Text) - (Convert.ToInt32(txtTeamASet3.Text))) == 2)
            //            {
            //                await DisplayAlert("", "Team   " + Common.newgame.Team2Name + "   has won the Game", "Ok");
            //                disabletaps();
            //                SaveSetsToDatabase(3, 1, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
            //                //save game
            //                saveToDataBase(2);
            //            }


            //        }

            //    }




            //}
            #endregion
        }

        private async void SaveSetsToDatabase(int SetId, int SetWinner, int TeamAPoints, int TeamBPoints)
        {
            try
            {
                SetsModel GS = new SetsModel()
                {
                    SetsID = SetId,
                    GameID = currentGameId,
                    SetWinner = SetWinner == 1 ? Common.newgame.Team1 : Common.newgame.Team2,
                    PlayTime = (Set1startTime - DateTime.Today).ToString(),
                    Team_1_points = TeamAPoints,
                    Team_2_points = TeamBPoints,
                    Replay = 0

                };
                await App.Database.SaveSetsAsync(GS);

                //save team A stat
                StatsModel SM1 = new StatsModel()
                {
                    SetID = SetId,
                    GameID = currentGameId,
                    TeamID = Common.newgame.Team1,
                    Points = TeamAPoints,
                    Serve = Convert.ToInt32(txtServeAPosints.Text),
                    Serve_Ace = ServeA_ace,
                    Serve_Pt = ServeA_pt,
                    Serve_Err = ServeA_err,
                    Attack = Convert.ToInt32(txtAttackAPosints.Text),
                    Attack_Err = AttackA_err,
                    Attack_Pt = AttackA_pt,
                    Attack_Kill = AttackA_kill,
                    Receive = Convert.ToInt32(txtReceiveAPosints.Text),
                    Receive_Err = ReceiveA_err,
                    Block = Convert.ToInt32(txtBlockAPosints.Text),
                    Block_Err = BlockA_err,
                    Block_Pt = BlockA_pt,
                    Fault = FaultAPoints
                };
                await App.Database.SaveStatsAsync(SM1);

                //save team A stat
                StatsModel SM2 = new StatsModel()
                {
                    SetID = SetId,
                    GameID = currentGameId,
                    TeamID = Common.newgame.Team2,
                    Points = TeamBPoints,
                    Serve = Convert.ToInt32(txtServeBPosints.Text),
                    Serve_Ace = ServeB_ace,
                    Serve_Pt = ServeB_pt,
                    Serve_Err = ServeB_err,
                    Attack = Convert.ToInt32(txtAttackBPosints.Text),
                    Attack_Err = AttackB_err,
                    Attack_Pt = AttackB_pt,
                    Attack_Kill = AttackB_kill,
                    Receive = Convert.ToInt32(txtReceiveBPosints.Text),
                    Receive_Err = ReceiveA_err,
                    Block = Convert.ToInt32(txtBlockBPosints.Text),
                    Block_Err = BlockB_err,
                    Block_Pt = BlockB_pt,
                    Fault = FaultBPoints
                };
                await App.Database.SaveStatsAsync(SM2);
            }
            catch (Exception)
            {

                return;
            }
        }

        //save game
        private async void saveToDataBase(int Winner)
        {
            GameFinished = true;
            try
            {
                GameModel GM = new GameModel()
                {
                    GameID = currentGameId,
                    Description = Common.newgame.Description,
                    GameWinner = Winner == 1 ? Common.newgame.Team1 : Common.newgame.Team2,
                    isCustom = Common.newgame.IsCustom,
                    isFF = false,
                    PlayDate = DateTime.Today,
                    PlayTime = (Set1startTime - DateTime.Today).ToString(),
                    TeamID_1 = Common.newgame.Team1,
                    TeamID_2 = Common.newgame.Team2

                };
                await App.Database.SaveGameAsync(GM);




            }
            catch (Exception)
            {

                return;
            }
        }

        private void disabletaps()
        {
            btnServeA.IsEnabled = false;
            btnServeB.IsEnabled = false;
            btnReceiveA.IsEnabled = false;
            btnReceiveB.IsEnabled = false;
            btnFaultA.IsEnabled = false;

            btnFaultB.IsEnabled = false;
            btnBlockA.IsEnabled = false;
            btnBlockB.IsEnabled = false;
            btnAttackA.IsEnabled = false;
            btnAttackB.IsEnabled = false;
        }

        private void ResetGameBoard()
        {
            txtServeAPosints.Text = "0";
            txtAttackAPosints.Text = "0";
            txtFaultAPosints.Text = "0";
            txtReceiveAPosints.Text = "0";
            txtBlockAPosints.Text = "0";

            txtServeBPosints.Text = "0";
            txtAttackBPosints.Text = "0";
            txtFaultBPosints.Text = "0";
            txtReceiveBPosints.Text = "0";
            txtBlockBPosints.Text = "0";

            ServeAPosints = 0;
            ServeBPosints = 0;
            AttackAPoints = 0;
            AttackBPoints = 0;
            ReceiveAPoints = 0;
            ReceiveBPoints = 0;
            BlockAPoints = 0;
            BlockBPoints = 0;
            FaultAPoints = 0;
            FaultBPoints = 0;



            if (Currentset == 1)
            {
                Currentset = 2;
                Set2startTime = DateTime.Now;
            }
            else if (Currentset == 2)
            {
                Currentset = 3;
                Set3startTime = DateTime.Now;
            }
        }

        private async void btnoption_Clicked(object sender, EventArgs e)
        {

            string timeouttxt = "";
            if (isfreezed)
            {
                timeouttxt = "Timeout (Stops time)";
            }
            else
            {
                timeouttxt = "End Timeout";
            }


            string action = await DisplayActionSheet("Options", "Cancel", null, "Forfeit set (start new set)", "Forfeit match", timeouttxt);

            if (action == "Forfeit set (start new set)")
            {
                string action2 = await DisplayActionSheet("Which team forfeits set?", "Cancel", null, Common.newgame.Team1Name, Common.newgame.Team2Name);

                

                if (action2 == Common.newgame.Team1Name)
                {
                    if (Currentset == 1)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet1.Text) - Convert.ToInt32(txtTeamBSet1.Text);

                       // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff ==0 || Adiff==1 ) && (Convert.ToInt32(txtTeamASet1.Text)>20 || Convert.ToInt32(txtTeamBSet1.Text) > 20))
                        {
                            txtTeamBSet1.Text = (Convert.ToInt32(txtTeamBSet1.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet1.Text = (Convert.ToInt32(txtTeamBSet1.Text) + (21 - Convert.ToInt32(txtTeamBSet1.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

                        }
                    }

                    else if (Currentset == 2)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet2.Text) - Convert.ToInt32(txtTeamBSet2.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet2.Text) > 20 || Convert.ToInt32(txtTeamBSet2.Text) > 20))
                        {
                            txtTeamBSet2.Text = (Convert.ToInt32(txtTeamBSet2.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet2.Text = (Convert.ToInt32(txtTeamBSet2.Text) + (21 - Convert.ToInt32(txtTeamBSet2.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

                        }
                    }
                    else if (Currentset == 3)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet3.Text) - Convert.ToInt32(txtTeamBSet3.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20))
                        {
                            txtTeamBSet3.Text = (Convert.ToInt32(txtTeamBSet3.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet3.Text = (Convert.ToInt32(txtTeamBSet3.Text) + (21 - Convert.ToInt32(txtTeamBSet3.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));

                        }

                        //decides who won the game
                        if (teamAWinCount > teamBWinCount)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();
                         
                            //save game
                            saveToDataBase(1);
                        }
                        else
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();
                           
                            //save game
                            saveToDataBase(2);
                        }


                    }



                }
                else if (action2 == Common.newgame.Team2Name)
                {
                    if (Currentset == 1)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet1.Text) - Convert.ToInt32(txtTeamASet1.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet1.Text) > 20 || Convert.ToInt32(txtTeamBSet1.Text) > 20))
                        {
                            txtTeamASet1.Text = (Convert.ToInt32(txtTeamASet1.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet1.Text = (Convert.ToInt32(txtTeamASet1.Text) + (21 - Convert.ToInt32(txtTeamASet1.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

                        }
                    }

                    else if (Currentset == 2)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet2.Text) - Convert.ToInt32(txtTeamASet2.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet2.Text) > 20 || Convert.ToInt32(txtTeamBSet2.Text) > 20))
                        {
                            txtTeamASet2.Text = (Convert.ToInt32(txtTeamASet2.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet2.Text = (Convert.ToInt32(txtTeamASet2.Text) + (21 - Convert.ToInt32(txtTeamASet2.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

                        }
                    }
                    else if (Currentset == 3)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet3.Text) - Convert.ToInt32(txtTeamASet3.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20))
                        {
                            txtTeamASet3.Text = (Convert.ToInt32(txtTeamASet3.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet3.Text = (Convert.ToInt32(txtTeamASet3.Text) + (21 - Convert.ToInt32(txtTeamASet3.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));

                        }

                        //decides who won the game
                        if (teamAWinCount > teamBWinCount)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(1);
                        }
                        else
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(2);
                        }


                    }

                }

            }
            else if (action == "Forfeit match")
            {
                string action3 = await DisplayActionSheet("Which team forfeits game?", "Cancel", null, "TEAM A", "TEAM B");
                if (action3 == Common.newgame.Team1Name)
                {
                    if (Currentset == 1)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet1.Text) - Convert.ToInt32(txtTeamBSet1.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet1.Text) > 20 || Convert.ToInt32(txtTeamBSet1.Text) > 20))
                        {
                            txtTeamBSet1.Text = (Convert.ToInt32(txtTeamBSet1.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet1.Text = (Convert.ToInt32(txtTeamBSet1.Text) + (21 - Convert.ToInt32(txtTeamBSet1.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

                        }
                    }

                    else if (Currentset == 2)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet2.Text) - Convert.ToInt32(txtTeamBSet2.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet2.Text) > 20 || Convert.ToInt32(txtTeamBSet2.Text) > 20))
                        {
                            txtTeamBSet2.Text = (Convert.ToInt32(txtTeamBSet2.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet2.Text = (Convert.ToInt32(txtTeamBSet2.Text) + (21 - Convert.ToInt32(txtTeamBSet2.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

                        }
                    }
                    else if (Currentset == 3)
                    {
                        int Adiff = Convert.ToInt32(txtTeamASet3.Text) - Convert.ToInt32(txtTeamBSet3.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20))
                        {
                            txtTeamBSet3.Text = (Convert.ToInt32(txtTeamBSet3.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamBSet3.Text = (Convert.ToInt32(txtTeamBSet3.Text) + (21 - Convert.ToInt32(txtTeamBSet3.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));

                        }

                        //decides who won the game
                        if (teamAWinCount > teamBWinCount)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(1);
                        }
                        else
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(2);
                        }


                    }



                }
                else if (action3 == Common.newgame.Team2Name)
                {
                    if (Currentset == 1)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet1.Text) - Convert.ToInt32(txtTeamASet1.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet1.Text) > 20 || Convert.ToInt32(txtTeamBSet1.Text) > 20))
                        {
                            txtTeamASet1.Text = (Convert.ToInt32(txtTeamASet1.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 1, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet1.Text = (Convert.ToInt32(txtTeamASet1.Text) + (21 - Convert.ToInt32(txtTeamASet1.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(1, 2, Convert.ToInt32(txtTeamASet1.Text), Convert.ToInt32(txtTeamBSet1.Text));

                        }
                    }

                    else if (Currentset == 2)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet2.Text) - Convert.ToInt32(txtTeamASet2.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet2.Text) > 20 || Convert.ToInt32(txtTeamBSet2.Text) > 20))
                        {
                            txtTeamASet2.Text = (Convert.ToInt32(txtTeamASet2.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet2.Text = (Convert.ToInt32(txtTeamASet2.Text) + (21 - Convert.ToInt32(txtTeamASet2.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(2, 2, Convert.ToInt32(txtTeamASet2.Text), Convert.ToInt32(txtTeamBSet2.Text));

                        }
                    }
                    else if (Currentset == 3)
                    {
                        int Adiff = Convert.ToInt32(txtTeamBSet3.Text) - Convert.ToInt32(txtTeamASet3.Text);

                        // IF the set is close, for example, 21:22 or 25:25, then Add + 2 point differential
                        if ((Adiff == 0 || Adiff == 1) && (Convert.ToInt32(txtTeamASet3.Text) > 20 || Convert.ToInt32(txtTeamBSet3.Text) > 20))
                        {
                            txtTeamASet3.Text = (Convert.ToInt32(txtTeamASet3.Text) + 2).ToString();

                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));
                        }
                        else //Adds 21 points to the team who won the set
                        {

                            txtTeamASet3.Text = (Convert.ToInt32(txtTeamASet3.Text) + (21 - Convert.ToInt32(txtTeamASet3.Text))).ToString();
                            teamAWinCount = 1;
                            ResetGameBoard();
                            SaveSetsToDatabase(3, 2, Convert.ToInt32(txtTeamASet3.Text), Convert.ToInt32(txtTeamBSet3.Text));

                        }

                        //decides who won the game
                        if (teamAWinCount > teamBWinCount)
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(1);
                        }
                        else
                        {
                            await DisplayAlert("", "Game is ended. Team " + Common.newgame.Team2Name + " has won the Game", "Ok");
                            disabletaps();

                            //save game
                            saveToDataBase(2);
                        }


                    }

                }
                await DisplayAlert("", "The game is concluded", "Ok");
                await Navigation.PopModalAsync();



            }

            else if (action == "Timeout (Stops time)")
            {
                isfreezed = true;

            }



        }

        

        protected override bool OnBackButtonPressed()
        {
            if (!GameFinished)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var res = await DisplayAlert("", "Do you want to abandon the game?", "Yes", "No");
                    if (res)
                    {
                        await Navigation.PopModalAsync();
                    }
                });
            }
            else
            {
                Navigation.PopModalAsync();
            }



            return true;
        }

        private void btnreplay_Clicked(object sender, EventArgs e)
        {
            releaseDoubleTap();
            releaseSingleTap();
        }
    }
}