﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNewTemaPage : ContentPage
    {
        public AddNewTemaPage()
        {
            InitializeComponent();
        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void btnAddteam_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTeamName.Text))
            {
                await DisplayAlert("", "Enter team name", "Ok");
                return;
            }

            if (string.IsNullOrEmpty(txtTeamName.Text))
            {
                await DisplayAlert("", "Enter player 1 name", "Ok");
                return;
            }

            if (string.IsNullOrEmpty(txtTeamName.Text))
            {
                await DisplayAlert("", "Enter player 2 name", "Ok");
                return;
            }

            Team team = new Team()
            {
                Name = txtTeamName.Text,
                PlayerName1 = txtPlayer1.Text,
                PlayerName2 = txtPlayer2.Text,
            };

            await App.Database.SaveTeamAsync(team);
            await DisplayAlert("", "Team Saved", "Ok");
            await Navigation.PopModalAsync();
        }
    }
}