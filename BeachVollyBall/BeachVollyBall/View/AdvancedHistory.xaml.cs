﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdvancedHistory : ContentPage
    {

        GameModel obj;
        public AdvancedHistory(Model.GameModel selectedObject)
        {
            InitializeComponent();
            obj = selectedObject;

            if (selectedObject != null)
            {
                txtTitle.Text = selectedObject.Team1Name + " VS "+ selectedObject.Team2Name;
                txtdescription.Text = selectedObject.Description;
                txtdate.Text = selectedObject.PlayDate.ToString("dd/MM/yyyy");
            }

            BindHistory();
        }

        private async void BindHistory()
        {

            
            //get Teams saved in database
            var teamdetails = await App.Database.GetTeamsAsync();
            //get Sets saved in database
            var Setdetails = await App.Database.GetSetsAsync();
            //get Stats saved in database
            var StatDetails = await App.Database.GetStatsAsync();
            
            //team A stat
            var statA = StatDetails.Where(x => x.GameID == obj.GameID && x.TeamID == obj.TeamID_1);
            //team B stats
            var statB = StatDetails.Where(x => x.GameID == obj.GameID && x.TeamID == obj.TeamID_2);


            //calculate set total
            var s = Setdetails.Where(x => x.GameID == obj.GameID);



            txtFinalScore.Text = s.Where(x => x.SetWinner == obj.TeamID_1).Count().ToString() + ":" + s.Where(x => x.SetWinner == obj.TeamID_2).Count().ToString();
            txtTeamAPoint.Text = s.Where(x => x.SetWinner == obj.TeamID_1).Count().ToString();
            txtTeamBPoint.Text = s.Where(x => x.SetWinner == obj.TeamID_2).Count().ToString();

            //game sets sum
            txtTeamAServe.Text = statA.Select(x => x.Serve).Sum().ToString();
            txtTeamBServe.Text = statB.Select(x => x.Serve).Sum().ToString();
            TeamAServePt.Text= statA.Select(x => x.Serve_Pt).Sum().ToString();
            TeamBServePt.Text = statB.Select(x => x.Serve_Pt).Sum().ToString();
            TeamAServeAce.Text = statA.Select(x => x.Serve_Ace).Sum().ToString();
            TeamBServeAce.Text = statB.Select(x => x.Serve_Ace).Sum().ToString();
            TeamAServeError.Text= statA.Select(x => x.Serve_Err).Sum().ToString();
            TeamBServeError.Text = statB.Select(x => x.Serve_Err).Sum().ToString();
            TeamAAttackAttempt.Text= statA.Select(x => x.Attack).Sum().ToString();
            TeamBAttackAttempt.Text = statB.Select(x => x.Attack).Sum().ToString();
            TeamAAttackPt.Text = statA.Select(x => x.Attack_Pt).Sum().ToString();
            TeamBAttackPt.Text = statB.Select(x => x.Attack_Pt).Sum().ToString();
            TeamAAttackKill.Text = statA.Select(x => x.Attack_Kill).Sum().ToString();
            TeamBAttackKill.Text = statB.Select(x => x.Attack_Kill).Sum().ToString();
            TeamAAttackError.Text = statA.Select(x => x.Attack_Err).Sum().ToString();
            TeamBAttackError.Text = statB.Select(x => x.Attack_Err).Sum().ToString();
            TeamARecieve.Text = statA.Select(x => x.Receive).Sum().ToString();
            TeamBRecieve.Text = statB.Select(x => x.Receive).Sum().ToString();
            TeamAReciveErr.Text = statA.Select(x => x.Receive_Err).Sum().ToString();
            TeamBReciveErr.Text = statB.Select(x => x.Receive_Err).Sum().ToString();
            TeamABlockAttempt.Text = statA.Select(x => x.Block).Sum().ToString();
            TeamBBlockAttempt.Text = statB.Select(x => x.Block).Sum().ToString();
            TeamABlockPoint.Text = statA.Select(x => x.Block_Pt).Sum().ToString();
            TeamBBlockPoint.Text = statB.Select(x => x.Block_Pt).Sum().ToString();
            TeamAErrorInPlay.Text = statA.Select(x => x.Error_InPlay).Sum().ToString();
            TeamBErrorInPlay.Text = statB.Select(x => x.Error_InPlay).Sum().ToString();

        }

        private void btnBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            if (obj != null)
            {
                bool res = await DisplayAlert("", "Confirm Delete?", "Yes", "No");
                if (res)
                {
                    try
                    {
                        busyIndi.IsVisible = true;
                        //delete Game
                        await App.Database.DeleteGameAsync(obj);

                        //delete sets
                        
                            await App.Database.DeleteSetsAsync(obj.GameID);
                        

                        //delete stats
                        
                            await App.Database.DeleteStatsAsync(obj.GameID);

                        busyIndi.IsVisible = false;
                        Navigation.PopModalAsync();
                    }
                    catch (Exception)
                    {

                        return;
                    }
                   

                }

            }
        }
    }
}