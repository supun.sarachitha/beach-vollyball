﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        public GamePage()
        {
            InitializeComponent();


            var tapGestureRecognizer1 = new TapGestureRecognizer();
            tapGestureRecognizer1.NumberOfTapsRequired = 2;
            tapGestureRecognizer1.Tapped += (s, e) => {
                DisplayAlert("", "TeamA serve tapped twice", "Ok");
            };
            frameserve.GestureRecognizers.Add(tapGestureRecognizer1);


            var tapGestureRecognizer2 = new TapGestureRecognizer();
            tapGestureRecognizer2.NumberOfTapsRequired = 1;
            tapGestureRecognizer2.Tapped += (s, e) => {
                DisplayAlert("", "TeamA serve tapped Once", "Ok");
            };
            frameserve.GestureRecognizers.Add(tapGestureRecognizer2);

        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }

        private void TapGestureRecognizer1_Tapped(object sender, EventArgs e)
        {

        }

        private void TapGestureRecognizer2_Tapped(object sender, EventArgs e)
        {

        }
    }
}