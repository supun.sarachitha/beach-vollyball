﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<NewGamePage>(this, "NewGame", async(sender) =>
            {
                busyIndi.IsVisible = true;
                await Navigation.PushModalAsync(new GamePage());
                busyIndi.IsVisible = false;
            });

            
        }

        private async void btnNewGame_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NewGamePage());
            
        }

        private async void btnTeams_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ViewTeams());
        }

        private async void btnHistory_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new HistroyPage());
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
        }
    }
}