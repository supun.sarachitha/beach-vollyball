﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewGamePage : ContentPage
    {
        public NewGamePage()
        {
            InitializeComponent();

           
            bindTeams();
        }

        private async void bindTeams()
        {
            //get Teams saved in database
            var res = await App.Database.GetTeamsAsync();
            //populate teams dropdowns
            txtTeam1.ItemsSource = res;
            txtTeam2.ItemsSource = res;


        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void btnCreate_Clicked(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtDescription.Text))
            {
                await DisplayAlert("", "Enter Description", "Ok");
                return;
            }

            if (txtTeam1.SelectedItem == null)
            {
                await DisplayAlert("", "Please select TEAM 1 and TEAM 2", "Ok");
                return;
            }

            if (txtTeam2.SelectedItem==null)
            {
                await DisplayAlert("", "Please select TEAM 1 and TEAM 2", "Ok");
                return;
            }

            if(((Team)txtTeam1.SelectedItem).TeamID == ((Team)txtTeam2.SelectedItem).TeamID)
            {
                await DisplayAlert("", "Select a Different team as team 2", "Ok");
                return;
            }

            Common.newgame = new CreateNewGameModel()
            {
                Description = txtDescription.Text,
                Team1 = ((Team)txtTeam1.SelectedItem).TeamID,
                Team2 = ((Team)txtTeam2.SelectedItem).TeamID,
                Team1Name= ((Team)txtTeam1.SelectedItem).Name,
                Team2Name = ((Team)txtTeam2.SelectedItem).Name,
                IsCustom = radCustom.IsChecked ? true : false
            };

            await Navigation.PopModalAsync();
            MessagingCenter.Send<NewGamePage>(this, "NewGame");
            
            
        }
    }
}