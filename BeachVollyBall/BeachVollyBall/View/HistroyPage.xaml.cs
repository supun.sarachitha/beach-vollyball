﻿using BeachVollyBall.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistroyPage : ContentPage
    {

         List<GameModel> gamedetails=null;
        public HistroyPage()
        {
            InitializeComponent();


            BindHistory();
        }

        private async void BindHistory()
        {

           /// await App.Database.GetHistoryAsync();
            //get History saved in database
            gamedetails = await App.Database.GetGamesAsync();
            //get Teams saved in database
            var teamdetails = await App.Database.GetTeamsAsync();
            //get Sets saved in database
            var Setdetails = await App.Database.GetSetsAsync();

            //assign team names
            foreach (var g in gamedetails)
            {
                g.Team1Name = teamdetails.Where(x => x.TeamID == g.TeamID_1).FirstOrDefault().Name;
                g.Team2Name = teamdetails.Where(x => x.TeamID == g.TeamID_2).FirstOrDefault().Name;

                //calculate set total
                var s = Setdetails.Where(x => x.GameID == g.GameID);

                g.setScore = s.Where(x => x.SetWinner == g.TeamID_1).Count().ToString() + ":" + s.Where(x => x.SetWinner == g.TeamID_2).Count().ToString();
            }

            





            //populate history list
            listview.ItemsSource = gamedetails;
            sort = false;
        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void HistoryItemTap_Tapped(object sender, EventArgs e)
        {
            Grid button = (Grid)sender;
            var selectedObject = (GameModel)button.BindingContext;

            await Navigation.PushModalAsync( new AdvancedHistory(selectedObject));
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (txtSearch.Text == "")
                {
                    BindHistory();
                }


                if (txtSearch.Text != null || txtSearch.Text.Trim() != "")
                {
                    var searchedResult = gamedetails.Where(x => x.Team1Name.ToLower().Contains(txtSearch.Text.ToLower()) || x.Team2Name.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
                    listview.ItemsSource = searchedResult;

                }
                else
                {
                    BindHistory();
                }
            }
            catch (Exception ex)
            {

            }
        }


        bool sort = false;
        private void btnSort_Clicked(object sender, EventArgs e)
        {
            if (!sort)
            {
                IconCarrot.Glyph = "\uf0de";
                listview.ItemsSource = gamedetails.OrderBy(x=>x.PlayDate);
                sort = true;
            }
            else
            {
                listview.ItemsSource = gamedetails.OrderByDescending(x => x.PlayDate);
                IconCarrot.Glyph = "\uf0dd";
                sort = false;
                BindHistory();
            }
            
        }
    }
}