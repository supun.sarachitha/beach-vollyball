﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BeachVollyBall.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistroyDetailPage : ContentPage
    {
        public HistroyDetailPage()
        {
            InitializeComponent();
        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}